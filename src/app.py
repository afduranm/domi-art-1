from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from config import config


from config import config

app=Flask (__name__)

conexion=MySQL(app)

@app.route('/cursos')
def listar_articulos():
    try:
        cursor=conexion.connection.cursor()
        sql="SELECT * FROM productos"
        cursor.execute(sql)
        datos=cursor.fetchall()
        articulos = []
        for fila in datos:
            articulo ={'codigo':fila[0],'nombre':fila[1],'precio_compra':fila[2],
                       'precio_venta':fila[3],'cantidad':fila[4]}
            articulos.append(articulo)
        return jsonify({'articulo':articulos, 'mensaje':"articulos listados"})
    except Exception as ex:
        return jsonify({'mensaje':"error"})
    
@app.route('/articulos/<codigo>',methods=['GET'])
def Leer_articulo(codigo):
    try:
        cursor=conexion.connection.cursor()
        sql="SELECT * FROM productos WHERE codigo = '{0}'".format(codigo)
        cursor.execute(sql)
        datos=cursor.fetchone()
        if datos !=  None:
             articulo ={'codigo':datos[0],'nombre':datos[1],'precio_compra':datos[2],
                       'precio_venta':datos[3],'cantidad':datos[4]}
             return jsonify({'articulos':articulo, 'mensaje':"articulo encontrado"})
        else:
            return jsonify({'mensaje':"articulo no encontrado"})         
    except Exception as ex:
        return jsonify({'mensaje':"error"})
@app.route('/articulos',methods=['POST'])
def Registrar_articulo():
    # print(request.json)
    try:
        cursor=conexion.connection.cursor()
        sql="""INSERT INTO productos (codigo,nombre,precio_de_compra,precio_de_venta,cantidad)
        VALUES('{0}','{1}','{2}','{3}','{4}')""".format(request.json['codigo'],request.json['nombre'],
        request.json['precio_de_compra'],request.json['precio_de_venta'],request.json['cantidad'],)
        cursor.execute(sql)
        conexion.connection.commit() #confirma la insersion
        return jsonify({'mensaje':"articulo registrado "})
    except Exception as ex:
        return jsonify({'mensaje':"error"})
def pagina_no_encontrada(error):
    return "<h1>la pagina que buscan no existe...</h1>",404

@app.route('/articulos/<codigo>',methods=['PUT'])
def Actualizar_articulo(codigo):
    # print(request.json)
    try:
        cursor=conexion.connection.cursor()
        sql="""UPDATE productos SET nombre='{0}',precio_de_compra='{1}',precio_de_venta='{2}',
        cantidad='{3}' WHERE codigo='{4}'""".format(request.json['nombre'],
        request.json['precio_de_compra'],request.json['precio_de_venta'],request.json['cantidad'],codigo)
        cursor.execute(sql)
        conexion.connection.commit() #confirma la insersion
        return jsonify({'mensaje':"articulo actualizado"})
    except Exception as ex:
        return jsonify({'mensaje':"error"})
def pagina_no_encontrada(error):
    return "<h1>la pagina que buscan no existe...</h1>",404


@app.route('/articulos/<codigo>',methods=['DELETE'])
def Eliminar_articulo(codigo):
    try:
        cursor=conexion.connection.cursor()
        sql="DELETE FROM productos WHERE codigo='{0}'".format(codigo)
        cursor.execute(sql)
        conexion.connection.commit()
        return jsonify({'mensaje':"articulo eliminado "})
    except Exception as ex:
        return jsonify({'mensaje':"error"})

    
def pagina_no_encontrada(error):
    return "<h1>La página que intentas buscar no existe ...</h1>"

if __name__=='__main__':
    app.config.from_object(config['development'])
    app.register_error_handler(404, pagina_no_encontrada)
    app.run()